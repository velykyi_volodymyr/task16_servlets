package com.velykyi;

import com.velykyi.domain.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet("/pizza-delivery")
public class PizzaServlet extends HttpServlet {
    private static Logger logger1 = LogManager.getLogger(PizzaServlet.class);
    private static Map<Integer, Pizza> order = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger1.info(this.getServletName() + " into doGet");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<h2>Your order</h2>");
        for (Pizza pizza : order.values()) {
            out.println("<p>" + pizza + "</p>");
        }
        out.println("<form action='pizza-delivery' method='POST'>\n"
                + " Pizza: <input type='text' name='pizza_name'>\n"
                + " Amount: <input type='text' name='pizza_amount'>\n"
                + " <button type='submit'>Add pizza</button>\n"
                + "</form>");

        out.println("<form action='pizza-delivery' method='PUT'>\n"
                + "Id : <input type='text' name='pizza_id'>\n"
                + " Pizza: <input type='text' name='pizza_name'>\n"
                + " Amount: <input type='text' name='pizza_amount'>\n"
                + " <button type='submit'>Update pizza</button>\n"
                + "</form>");
        out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger1.info(this.getServletName() + " into doPost");
        String newPizzaName = req.getParameter("pizza_name");
        int newAmount = Integer.parseInt(req.getParameter("pizza_amount"));
        Pizza newPizza = new Pizza(newPizzaName, newAmount);
        order.put(newPizza.getId(), newPizza);
        doGet(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger1.info(this.getServletName() + " into doPut");
        int pizzaId = Integer.parseInt(req.getParameter("pizza_id"));
        String pizzaName = req.getParameter("pizza_name");
        int pizzaAmount = Integer.parseInt(req.getParameter("pizza_amount"));
        Pizza pizza = order.get(pizzaId);
        if (pizzaName == null) {
            if (pizzaAmount != 0) {
                pizza.setAmount(pizzaAmount);
            }
        } else {
            pizza.setPizzaName(pizzaName);
            pizza.setAmount(pizzaAmount);
        }
        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        logger1.info("Servlet " + this.getServletName() + " has started.");
    }

    @Override
    public void destroy() {
        logger1.info("Servlet " + this.getServletName() + " has destroyed.");
    }
}
