package com.velykyi.domain;

public class Pizza {
    private static int unique = 1;
    private String pizzaName;
    private int amount;
    private int id;

    public Pizza(String pizzaName, int amount) {
        this.pizzaName = pizzaName;
        this.amount = amount;
        id = unique;
        unique++;
    }

    public int getId() {
        return id;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return this.id + " " + this.pizzaName + " amount: " + this.amount;
    }
}
